package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;

	/**
	 * Find all {@link com.citi.training.employees.model.Department}s in the database or HTTP 404.
	 */
	public List<Trade> findAll() {
		return tpl.query("SELECT id, stock, price, volume FROM trade", new TradeMapper());
	}
	/**
	 * Delete an {@link com.citi.training.employees.model.Department} in the database by its integer id.
	 */
	public  void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM trade where id=?", id);
	}
	/**
	 * Find an {@link com.citi.training.employees.model.Trade} in the database by its integer id or HTTP 404.
	 */
	public Trade findById(int id) {
		List<Trade> trades = tpl.query(
				"SELECT id, stock, price, volume FROM trade WHERE id=?",
				new Object [] {id},
				new TradeMapper());
		if(trades.size() <= 0) {
			throw new TradeNotFoundException("Trade with id=[" + id +
					"] not found");
		}
		return trades.get(0);

	}
	/**
	 * Create an {@link com.citi.training.employees.model.Employee} in the database.
	 */
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tpl.update(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps =
								connection.prepareStatement("insert into trade (stock, price, volume) values (?,?,?)",
										Statement.RETURN_GENERATED_KEYS);
						ps.setString(1, trade.getStock());
						ps.setDouble(2, trade.getPrice());
						ps.setInt(3, trade.getVolume());
						
						
						return ps;

					}
				},
				keyHolder
				);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}


	private static final class TradeMapper implements RowMapper<Trade>{
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"),
					rs.getString("stock"),
					rs.getDouble("price"),
					rs.getInt("volume")
					);
		}
	}




}
