package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.dao.MysqlTradeDao;
import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

public class MysqlTradeDaoTests {


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTests {

	int invalidid = 112;
	
	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	@Test
	@Transactional //if you have multiple queries within function and you want them all to pass or fail as a unit i.e. one fails they all fail as a unit
	public void test_createandFindAll() {
		mysqlTradeDao.create(new Trade(-1, "ARPS", 10.0, invalidid));
		
		assertEquals(mysqlTradeDao.findAll().size(),1);
	}
	
	@Test
	@Transactional
	public void test_findById() {
		Trade trade1 = new Trade(-2, "GOOGL", 10.0, 2);
		mysqlTradeDao.create(trade1);
		assertEquals(mysqlTradeDao.findById(trade1.getId()).getStock(),trade1.getStock());
			
	}
	
	 @Test(expected = TradeNotFoundException.class)
	    public void test_getemployeeNotFound() {
	    	
	    mysqlTradeDao.findById(invalidid);
	    	
	    }
	  @Test(expected = TradeNotFoundException.class)
	    public void test_removeemployee() {
	    	
	    	mysqlTradeDao.deleteById(invalidid);
	    }
		
		
		
	}
	
}
