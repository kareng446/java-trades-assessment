package com.citi.training.trades.dao;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

public class InMemTradeDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemTradeDaoTests.class);

    private int testId = 9;
    private String testStock = "WGLA";
    private double testPrice = 123;
	private int testVolume = 2;

    @Test
    public void test_saveAndGetEmployee() {
        Trade testTrade = new Trade(-1, testStock, testPrice,testVolume);
        InMemTradeDao testRepo = new InMemTradeDao();

        testTrade = testRepo.create(testTrade);
 
        assertTrue(testRepo.findById(testTrade.getId()).equals(testTrade));

    }

    @Test
    public void test_saveAndGetAllTrades() {
    	Trade[] testTradeArray = new Trade[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for(int i=0; i<testTradeArray.length; ++i) {
            testTradeArray[i] = new Trade(-1, testStock, testPrice, testVolume);

            testRepo.create(testTradeArray[i]);
        }

        List<Trade> returnedTrades = testRepo.findAll();
        LOG.info("Received [" + returnedTrades.size() +
                 "] Employees from repository");

        for(Trade thisTrade: testTradeArray) {
            assertTrue(returnedTrades.contains(thisTrade));
        }
        LOG.info("Matched [" + testTradeArray.length + "] Employees");
    }

    @Test (expected = TradeNotFoundException.class)
    public void test_deleteTrade() {
    	Trade[] testTradeArray = new Trade[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for(int i=0; i<testTradeArray.length; ++i) {
            testTradeArray[i] = new Trade(testId + i,testStock, testPrice, testVolume);

            testRepo.create(testTradeArray[i]);
        }
        Trade removedEmployee = testTradeArray[5];
    	testRepo.deleteById(removedEmployee.getId());
    	LOG.info("Removed item from Employee array");
    	testRepo.findById(removedEmployee.getId());
    }
}

